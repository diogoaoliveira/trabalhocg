package baseCG;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class AnisotropicFrame extends JFrame
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new AnisotropicFrame();
	}

	AnisotropicFrame()
	{
		super("Modo de mapeamento anisotr�pico.");
		addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			}
		);
		
		setSize(400, 300);
		add("Center", new CvAnisotropic());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}

@SuppressWarnings("serial")
class CvAnisotropic extends Canvas
{
	int maxX;
	int maxY;
	float pixelWidth;
	float pixelHeight;
	float rWidth = 10F;
	float rHeight = 7.5F;
	float xP = -1;
	float yP;
	
	CvAnisotropic()
	{
		addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e)
				{
					xP = fx(e.getX());
					yP = fy(e.getY());
					repaint();
				}
			}
		);
	}
	
	void initGr()
	{
		Dimension d = getSize();
		maxX = d.width - 1;
		maxY = d.height - 1;
		pixelWidth = rWidth / maxX;
		pixelHeight = rHeight / maxY;
	}
	
	int iX(float x)
	{
		return Math.round(x / pixelWidth);
	}
	
	int iY(float y)
	{
		return maxY - Math.round(y / pixelHeight);
	}
	
	float fx(int x)
	{
		return x * pixelWidth;
	}
	
	float fy(int y)
	{
		return (maxY - y) * pixelHeight;
	}
	
	public void paint(Graphics g)
	{
		initGr();
		int left = iX(0);
		int right = iX(rWidth);
		int bottom = iY(0);
		int top = iY(rHeight);
		
		if(xP >= 0)
		{
			g.drawString("Coordenadas l�gicas do ponto selecionado: " + xP + " " + yP, 20, 100);
		}
		g.setColor(Color.red);
		g.drawLine(left, bottom, right, bottom);
		g.drawLine(right, bottom, right, top);
		g.drawLine(right, top, left, top);
		g.drawLine(left, top, left, bottom);
	}
}