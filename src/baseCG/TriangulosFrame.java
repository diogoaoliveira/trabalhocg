package baseCG;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class TriangulosFrame extends JFrame
{
	public TriangulosFrame()
	{
		super("Checkers");
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent e)
					{
						System.exit(0);
					}
				}
			);
		setSize(600, 400);
		add("Center", new CvTriangulos());
		setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new TriangulosFrame();
	}
}

@SuppressWarnings("serial")
class CvTriangulos extends Canvas
{
	int maxX, maxY, minMaxXY, xCenter, yCenter;
	
	void initGr()
	{
		Dimension d = getSize();
		maxX = d.width - 1;
		maxY = d.height - 1;
		minMaxXY = Math.min(maxX, maxY);
		xCenter = maxX / 2;
		yCenter = maxY / 2;
	}
	public void paint(Graphics g)
	{
		initGr();
		float side = 0.95F * minMaxXY;
		float sideHalf = 0.5F * side;
		float h = sideHalf * (float)Math.sqrt(3);
		
		float xA, yA, xB, yB, xC, yC;
		float xA1, yA1, xB1, yB1, xC1, yC1;
		float p, q;
		
		q = 0.05F;
		p = 1 - q;
		
		xA = xCenter - sideHalf;
		yA = yCenter - 0.5F * h;
		xB = xCenter + sideHalf;
		yB = yA;
		xC = xCenter;
		yC = yCenter + 0.5F * h;
		
		for(int i = 0; i < 50; i++)
		{
			g.drawLine(iX(xA), iY(yA), iX(xB), iY(yB));
			g.drawLine(iX(xB), iY(yB), iX(xC), iY(yC));
			g.drawLine(iX(xC), iY(yC), iX(xA), iY(yA));
			
			xA1 = p * xA + q * xB;
			yA1 = p * yA + q * yB;
			xB1 = p * xB + q * xC;
			yB1 = p * yB + q * yC;
			xC1 = p * xC + q * xA;
			yC1 = p * yC + q * yA;
			xA = xA1;
			xB = xB1;
			xC = xC1;
			yA = yA1;
			yB = yB1;
			yC = yC1;
		}
	}
	
	int iX(float x)
	{
		return Math.round(x);
	}
	
	int iY(float y)
	{
		return maxY - Math.round(y);
	}
	
	float fX(int x)
	{
		return (float)x;
	}
	
	float fY(int y)
	{
		return (float)(maxY - y);
	}
}