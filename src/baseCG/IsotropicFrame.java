package baseCG;

// O modo de mapeamento isotr�pico
// Origem do sistema de coordenadas l�gicas no centro da �rea de desenho
// Eixo y positivo para cima. Quadrado (girado 45 graus) cabe na �rea de 
// desenho. O click do mouse exibe as coodenadas l�gicas do ponto selecionado.

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class IsotropicFrame extends JFrame
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new IsotropicFrame();
	}

	IsotropicFrame()
	{
		super("Modo de mapeamento isotr�pico.");
		addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			}
		);
		
		setSize(400, 300);
		add("Center", new CvIsotropic());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}

@SuppressWarnings("serial")
class CvIsotropic extends Canvas
{
	int centerX;
	int centerY;
	float pixelSize;
	float rWidth = 10F;
	float rHeight = 10F;
	float xP = 1000000;
	float yP;
	
	CvIsotropic()
	{
		addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e)
				{
					xP = fx(e.getX());
					yP = fy(e.getY());
					repaint();
				}
			}
		);
	}
	
	void initGr()
	{
		Dimension d = getSize();
		int maxX = d.width - 1;
		int maxY = d.height - 1;
		pixelSize = Math.max(rWidth / maxX, rHeight / maxY);
		centerX = maxX / 2;
		centerY = maxY / 2;
	}
	
	int iX(float x)
	{
		return Math.round(centerX + x / pixelSize);
	}
	
	int iY(float y)
	{
		return Math.round(centerY - y / pixelSize);
	}
	
	float fx(int x)
	{
		return (x - centerX) * pixelSize;
	}
	
	float fy(int y)
	{
		return (centerY - y) * pixelSize;
	}
	
	public void paint(Graphics g)
	{
		initGr();
		int left = iX(-rWidth / 2);
		int right = iX(rWidth / 2);
		int bottom = iY(-rHeight / 2);
		int top = iY(rHeight / 2);
		int xMiddle = iX(0);
		int yMiddle = iY(0);
		
		if(xP != 1000000)
		{
			g.drawString("Coordenadas l�gicas do ponto selecionado: x:" + xP + " y:" + yP, 20, 100);
		}
		g.setColor(Color.red);
		g.drawLine(xMiddle, bottom, right, yMiddle);
		g.drawLine(right, yMiddle, xMiddle, top);
		g.drawLine(xMiddle, top, left, yMiddle);
		g.drawLine(left, yMiddle, xMiddle, bottom);
	}
}